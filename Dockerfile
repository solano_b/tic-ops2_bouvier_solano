# Use a smaller base image
FROM python:3.11-alpine

# Set the working directory
WORKDIR /app

# Copy only the requirements file first (to leverage Docker cache)
COPY ./OPS2_API/requirements.txt /app/requirements.txt

# Create a virtual environment and set PATH
RUN python3 -m venv /home/venv/
ENV PATH="/home/venv/bin:$PATH"

# Install Python dependencies
RUN pip3 install --no-cache-dir -r requirements.txt

# Copy the rest of the application code
COPY ./OPS2_API /app

# Create Entrypoint
ENTRYPOINT [ "uvicorn", "main:app" ]

# Start the API
CMD ["--host", "0.0.0.0", "--port", "4242"]
