import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from fastapi.testclient import TestClient
from main import app
from models.role_model import Role
from src.database import SessionLocal

client = TestClient(app)


def test_get_roles():
    """Test for the route to get all roles"""
    response = client.get("/roles/")
    assert response.status_code == 200
    assert len(response.json()) > 0


def test_post_role():
    """Test for the route to create one role"""
    response = client.post("/role/add", json={"name": "new_role"})
    assert response.status_code == 200
    assert response.json() == {"message": "Role added successfully"}

    # Verify that the role has been added to the database
    with SessionLocal() as db:
        role = db.query(Role).filter(Role.name == "new_role").first()
        assert role is not None


def test_post_role_error():
    """Test for the route to create one role with an error"""
    response = client.post("/role/add", json="new_role_error")
    assert response.status_code == 422

    # Verify that the role has not been added to the database
    with SessionLocal() as db:
        role = db.query(Role).filter(Role.name == "new_role_error").first()
        assert role is None


def test_post_roles():
    """Test for the route to create multiple roles"""
    response = client.post("/roles/add", json=["role1", "role2"])
    assert response.status_code == 200
    assert response.json() == {"message": "Roles added successfully"}

    # Verify that the roles have been added to the database
    with SessionLocal() as db:
        roles = db.query(Role).filter(Role.name.in_(["role1", "role2"])).all()
        assert len(roles) == 2


def test_post_roles_error():
    """Test for the route to create multiple roles with an error"""
    response = client.post("/roles/add", json={"role_names": ["role3", "role4"]})
    assert response.status_code == 422

    # Verify that the roles have not been added to the database
    with SessionLocal() as db:
        roles = db.query(Role).filter(Role.name.in_(["role3", "role4"])).all()
        assert roles == []


def test_update_role():
    """Test for the route to update a role"""
    response = client.post("/role/update?role_name=new_role&new_name=updated_role")
    assert response.status_code == 200
    assert response.json() == {
        "message": "Role 'new_role' updated successfully to 'updated_role'"
    }

    # Verify that the role has been updated in the database
    with SessionLocal() as db:
        role = db.query(Role).filter(Role.name == "new_role").first()
        assert role is None
        role = db.query(Role).filter(Role.name == "updated_role").first()
        assert role is not None


def test_update_role_error():
    """Test for the route to update a role with an error"""
    response = client.post(
        "/role/update",
        json={"role_name": "updated_role", "new_name": "updated_role_error"},
    )
    assert response.status_code == 422

    # Verify that the role has not been modified in the database
    with SessionLocal() as db:
        role = db.query(Role).filter(Role.name == "updated_role").first()
        assert role is not None
        role = db.query(Role).filter(Role.name == "updated_role_error").first()
        assert role is None


def test_delete_role():
    """Test for the route to delete a role"""
    response = client.post("/role/delete?role_name=updated_role")
    assert response.status_code == 200
    assert response.json() == {"message": "Role 'updated_role' deleted successfully"}

    response = client.post("/role/delete?role_name=role1")
    assert response.status_code == 200
    assert response.json() == {"message": "Role 'role1' deleted successfully"}

    response = client.post("/role/delete?role_name=role2")
    assert response.status_code == 200
    assert response.json() == {"message": "Role 'role2' deleted successfully"}

    # Verify that the roles have been deleted from the database
    with SessionLocal() as db:
        roles = (
            db.query(Role)
            .filter(Role.name.in_(["updated_role", "role1", "role2"]))
            .all()
        )
        assert roles == []


def test_delete_role_error():
    """Test for the route to delete a role with an error"""
    response = client.post("/role/delete", json="client")
    assert response.status_code == 422

    # Verify that the role has not been deleted from the database
    with SessionLocal() as db:
        role = db.query(Role).filter(Role.name == "client").first()
        assert role is not None
