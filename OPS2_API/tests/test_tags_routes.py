import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from fastapi.testclient import TestClient
from main import app
from models.tag_model import Tag
from src.database import SessionLocal

client = TestClient(app)


def test_get_tags():
    """Test get all tags route"""
    response = client.get("/tags/")
    assert response.status_code == 200
    assert len(response.json()) > 0


def test_post_tag():
    """Test create one tag route"""
    response = client.post("/tag/add", json={"name": "new_tag"})
    assert response.status_code == 200
    assert response.json() == {"message": "Tag added successfully"}

    # Verify that the tag has been added to the database
    with SessionLocal() as db:
        tag = db.query(Tag).filter(Tag.name == "new_tag").first()
        assert tag is not None


def test_post_tag_error():
    """Test create one tag route with error"""
    response = client.post("/tag/add", json="new_tag_error")
    assert response.status_code == 422

    # Verify that the tag has not been added to the database
    with SessionLocal() as db:
        tag = db.query(Tag).filter(Tag.name == "new_tag_error").first()
        assert tag is None


def test_post_tags():
    """Test create multiple tags route"""
    response = client.post("/tags/add", json=["tag1", "tag2"])
    assert response.status_code == 200
    assert response.json() == {"message": "Tags added successfully"}

    # Verify that the tags have been added to the database
    with SessionLocal() as db:
        tags = db.query(Tag).filter(Tag.name.in_(["tag1", "tag2"])).all()
        assert len(tags) == 2


def test_post_tags_error():
    """Test create multiple tags route with error"""
    response = client.post("/tags/add", json={"tag_names": ["tag3", "tag4"]})
    assert response.status_code == 422

    # Verify that the tags have not been added to the database
    with SessionLocal() as db:
        tags = db.query(Tag).filter(Tag.name.in_(["tag3", "tag4"])).all()
        assert tags == []


def test_update_tag():
    """Test update a tag route"""
    response = client.post("/tag/update?tag_name=new_tag&new_name=updated_tag")
    assert response.status_code == 200
    assert response.json() == {
        "message": "Tag 'new_tag' updated successfully to 'updated_tag'"
    }

    # Verify that the tag has been updated in the database
    with SessionLocal() as db:
        tag = db.query(Tag).filter(Tag.name == "new_tag").first()
        assert tag is None
        tag = db.query(Tag).filter(Tag.name == "updated_tag").first()
        assert tag is not None


def test_update_tag_error():
    """Test update a tag route with error"""
    response = client.post(
        "/tag/update",
        json={"tag_name": "updated_tag", "new_name": "updated_tag_error"},
    )
    assert response.status_code == 422

    # Verify that the tag has not been modified in the database
    with SessionLocal() as db:
        tag = db.query(Tag).filter(Tag.name == "updated_tag").first()
        assert tag is not None
        tag = db.query(Tag).filter(Tag.name == "updated_tag_error").first()
        assert tag is None


def test_delete_tag():
    """Test delete a tag route"""
    response = client.post("/tag/delete?tag_name=updated_tag")
    assert response.status_code == 200
    assert response.json() == {"message": "Tag 'updated_tag' deleted successfully"}

    response = client.post("/tag/delete?tag_name=tag1")
    assert response.status_code == 200
    assert response.json() == {"message": "Tag 'tag1' deleted successfully"}

    response = client.post("/tag/delete?tag_name=tag2")
    assert response.status_code == 200
    assert response.json() == {"message": "Tag 'tag2' deleted successfully"}

    # Verify that the tags have been deleted from the database
    with SessionLocal() as db:
        tags = db.query(Tag).filter(Tag.name.in_(["updated_tag", "tag1", "tag2"])).all()
        assert tags == []


def test_delete_tag_error():
    """Test delete a tag route with error"""
    response = client.post("/tag/delete", json="client")
    assert response.status_code == 422

    # Verify that the tag has not been deleted from the database
    with SessionLocal() as db:
        tag = db.query(Tag).filter(Tag.name == "client").first()
        assert tag is not None
