# TIC-OPS2
**Loïc Bouvier & Benjamin Solano**

### Source Code

[![Static Badge](https://img.shields.io/badge/OPS2--API--black-black?style=flat&logo=github&label=Github%20Project)](https://github.com/Elie-Renneson/OPS2-API)

-------------------
###  Project Status

[![pipeline status](https://gitlab.com/solano_b/tic-ops2_bouvier_solano/badges/main/pipeline.svg?key_text=Pipeline+Status&key_width=100)](https://gitlab.com/solano_b/tic-ops2_bouvier_solano/-/commits/main)
[![coverage report](https://gitlab.com/solano_b/tic-ops2_bouvier_solano/badges/main/coverage.svg?key_text=Code+Coverage&key_width=100)](https://gitlab.com/solano_b/tic-ops2_bouvier_solano/-/commits/main)
[![Latest Release](https://gitlab.com/solano_b/tic-ops2_bouvier_solano/-/badges/release.svg)](https://gitlab.com/solano_b/tic-ops2_bouvier_solano/-/releases)


[![python](https://img.shields.io/badge/Python-3.11-3776AB.svg?style=flat&logo=python&logoColor=white)](https://www.python.org)
[![FastAPI](https://img.shields.io/badge/FastAPI-0.104.1-009688.svg?style=flat&logo=FastAPI&logoColor=white)](https://fastapi.tiangolo.com)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Runners

The project uses two runners : a Docker runner for lint / build and test stages and a shell runner for app deployment stage. They are both running on a Google Compute Engine vm.

## Pipeline stages

The pipeline consists of 4 stages:
- lint
- build
- test
- deploy

**Lint** has one job that uses black linter.

Depending on the branch, the **build** stage only builds or builds and push the result using Docker and Kaniko (default branch).

The **test** stage (non-default branch) uses pytest to test and generate coverage reports both in the pipeline terminal and to xml and html files pushed as artifacts to Gitlab.

The **deploy** stage consists of two jobs : a job for non-default branches that publises the coverage reports and a manual deploy to production jobs that deploys the app on the vm.

## Variables

The deploy to production job requires the variables templated in ".env.sample" to be added to the gitlab UI on protected mode.
The test job requires the following variables to be added to gitlab UI as well:

    DATABASE_HOST_TEST
    DATABASE_PORT_TEST
    DATABASE_NAME_TEST
    DATABASE_USER_TEST
    DATABASE_PASSWORD_TEST
    POSTGRES_DB_TEST
    POSTGRES_USER_TEST
    POSTGRES_PASSWORD_TEST
    POSTGRES_HOST_AUTH_METHOD_TEST

## Coverage report

https://solano_b.gitlab.io/tic-ops2_bouvier_solano/

## Difficulties

Avoiding committing env variables in the repo ended up being quite tricky. Differenciating the variables used in the test job from the production ones revealed to be difficult because with such minimal configuration, postgres expects them to be named exactly the same and gitlab UI variables take precedence over variables declared in the yaml.

For this reason, the workaround we found was to enable the test job on non-default branches only, this way, Gitlab could only access the test variables and not mix them up with the production ones.
